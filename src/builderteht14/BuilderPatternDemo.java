/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builderteht14;

/**
 *
 * @author aleks
 */
public class BuilderPatternDemo {
   public static void main(String[] args) {
   
      MealBuilder mealBuilder = new MealBuilder();

      Meal MC = mealBuilder.prepareMCBurger();
      System.out.println("McDonalds Burger");
      MC.showItems();
      System.out.println("Total Cost: " + MC.getCost());

      Meal BK = mealBuilder.prepareBKBurger();
      System.out.println("\n\nBurger King Burger");
      BK.showItems();
      System.out.println("Total Cost: " + BK.getCost());
   }
}
