/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builderteht14;

/**
 *
 * @author aleks
 */
public class MealBuilder {

   public Meal prepareBKBurger (){
      Meal meal = new Meal();
      meal.addItem(new BKBurger());
      meal.addItem(new Lettuce());
      meal.addItem(new Cheese());
      meal.addItem(new Onion());
      return meal;
   }   

   public Meal prepareMCBurger (){
      Meal meal = new Meal();
      meal.addItem(new McDonaldsBurger());
      meal.addItem(new Lettuce());
      meal.addItem(new Cheese());
      meal.addItem(new Onion());
      
      return meal;
   }
}
